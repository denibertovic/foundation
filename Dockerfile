FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

# Install required Ubuntu packages
RUN apt-get update  \
 && apt-get install -y --no-install-recommends \
    apt-transport-https \
    build-essential \
    ca-certificates \
    curl \
    dirmngr \
    git \
    gpg \
    gpg-agent \
    iputils-ping \
    iputils-tracepath \
    jq \
    libc-dev \
    libgmp-dev \
    libicu-dev \
    libpq-dev \
    libpq-dev \
    libtinfo-dev \
    locales \
    netbase \
    pkg-config \
    software-properties-common \
    tzdata \
    unzip \
    vim \
    zlib1g-dev \
 && apt-get clean  \
 && rm -rf /var/lib/apt/lists/*  \
 && jq --version

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
RUN update-locale en_US.UTF-8

# Install latest version of Stack
ENV STACK_VERSION=1.9.3
ENV HASKELL_TOOL_RESOLVER=lts-12.24
RUN curl -sSL https://github.com/commercialhaskell/stack/releases/download/v$STACK_VERSION/stack-$STACK_VERSION-linux-x86_64.tar.gz | tar xz --wildcards --strip-components=1 -C /usr/local/bin '*/stack'
RUN export STACK_ROOT=/usr/local/lib/stack && \
    stack --install-ghc --resolver=$HASKELL_TOOL_RESOLVER install --local-bin-path=/usr/local/bin alex happy && \
    cd $STACK_ROOT && \
    find . -type f -not -path './snapshots/*/share/*' -and -not -name 'template-hsc.h' -exec rm '{}' \; && \
    find . -type d -print0 |sort -rz |xargs -0 rmdir 2>/dev/null || true

ENV NODEJS_VERSION=10.15.1
RUN mkdir /usr/local/lib/nodejs && \
    curl -sSL "https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.xz" | tar -xJvf - -C /usr/local/lib/nodejs && \
    mv /usr/local/lib/nodejs/node-v${NODEJS_VERSION}-linux-x64 /usr/local/lib/nodejs/node-${NODEJS_VERSION}

ENV NODEJS_HOME=/usr/local/lib/nodejs/node-${NODEJS_VERSION}/bin
ENV PATH=$NODEJS_HOME:$PATH

ENV PULP_VERSION=13.0.0
RUN npm -g install pulp@${PULP_VERSION}

ENV PSC_PACKAGE_VERSION=0.5.1
RUN curl -sSL https://github.com/purescript/psc-package/releases/download/v${PSC_PACKAGE_VERSION}/linux64.tar.gz | tar xzvf - --wildcards --strip-components=1 -C /usr/local/bin '*/psc-package'

ENV PURESCRIPT_VERSION=0.13.3
RUN curl -sSL https://github.com/purescript/purescript/releases/download/v${PURESCRIPT_VERSION}/linux64.tar.gz | tar xzvf - --wildcards --strip-components=1 -C /usr/local/bin '*/purs'

ENV SPAGO_VERSION=0.10.0.0
RUN curl -sSL "https://github.com/spacchetti/spago/releases/download/${SPAGO_VERSION}/linux.tar.gz" | tar xzf - -C /usr/local/bin

# Install PID1
ENV PID1_VERSION=0.1.2.0
RUN curl -sSL "https://github.com/fpco/pid1/releases/download/v${PID1_VERSION}/pid1-${PID1_VERSION}-linux-x86_64.tar.gz" | tar xzf - -C /usr/local && \
    chown root:root /usr/local/sbin && \
    chown root:root /usr/local/sbin/pid1

# Install fpco/cache-s3
ENV CACHE_S3_VERSION=0.1.7
RUN curl -sSL https://github.com/fpco/cache-s3/releases/download/v${CACHE_S3_VERSION}/cache-s3-v${CACHE_S3_VERSION}-linux-x86_64.tar.gz | tar xz -C /usr/local/bin 'cache-s3'

ENV PURESCRIPT_PSA_VERSION=0.7.3
RUN npm -g install purescript-psa@${PURESCRIPT_PSA_VERSION}

ENV PURESCRIPT_SUGGEST_VERSION=2.2.0
RUN npm -g install purescript-suggest@${PURESCRIPT_SUGGEST_VERSION}

ENV PSCID_VERSION=2.8.5
RUN npm -g install pscid@${PSCID_VERSION}

ENV PARCEL_VERSION=1.12.4
RUN npm install -g parcel-bundler

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod +x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

CMD ["/bin/bash"]
