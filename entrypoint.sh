#!/bin/bash

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback
USER_ID=${LOCAL_USER_ID:-9001}
APP_DIR=${APP_DIR:-/opt/app}

echo "Starting with UID : $USER_ID"
useradd --shell /bin/bash -u $USER_ID -o -c "" -m user
export HOME=/home/user

# create dir if it doesn't already exist
# This is for compatibility
mkdir -p $APP_DIR
# set correct permissions on APP_DIR and subfolders
chown -R user. $APP_DIR

exec /usr/local/sbin/pid1 -u user -g user "$@"
